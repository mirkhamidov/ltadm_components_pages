<?
$items=array(
    // "pages_config" => array(
    //     'desc'         => 'Конфигурация для страницы',
    //     'type'         => 'pages_config',
    //     "not_in_table" => true,
    //     "no_condition" => true,
    //     "no_order"     => true,
    //     "no_store"     => true,
    // ),
  "page_type" => array(
         "desc" => "Тип страницы",
         "type" => "radio",

         "select_desc" => "",

         "full_desc" => "Используется для определения типа страницы и места хранения данных",

         "values" => array ("static" => "Статическая", "dynamic" => "Динамическая", "external" => "Внешняя", "node" => "Без содержания|JavaScript"),
         "switch" => array (
                   "static" => array(
                     'enabled' => array("content","unixtime","name","name_menu","url","name_title","classification","descrip","keywords","id_lt_group_templates","target"),
                     'disabled' => array("content_top","file","content_bottom"),
                   ),
                   "dynamic" => array(
                     'enabled' => array("content_top","file","content_bottom","name","name_menu","url","name_title","classification","descrip","keywords","id_lt_group_templates","target"),
                     'disabled' => array("content","unixtime"),
                   ),
                   "external" => array(
                     'enabled' => array("name","name_menu","url","target"),
                     'disabled' => array("content","unixtime","content_top","file","content_bottom","name_title","classification","descrip","keywords","id_lt_group_templates"),
                   ),
                   "node" => array(
                     'disabled' => array("content","unixtime","content_top","file","content_bottom","name_title","classification","descrip","keywords","id_lt_group_templates","target"),
                   ),
         ),
         "default_value" => "static",
         "select_on_edit" => true,
         "select_on_edit_disabled" => true,
       ),



  "id_lt_group_templates" => array(
         "desc" => "Используемый шаблон",
         "type" => "select_from_table",

         "select_desc" => 'для всех типов страниц, кроме "Без содержания"',

         "full_desc" => "Используется для определения оформления страницы для выдачи пользователю",

         "table" => "lt_group_templates",
         "key_field" => "id_lt_group_templates",
         "fields" => array("name"),
         "show_field" => "%1",
         "condition" => "",
         "order" => array ("name" => "ASC"),
         "select_on_edit" => true,
       ),
  "id_lt_languages" => array(
         "desc" => "Язык страницы",
         "type" => "select_from_table",
         "table" => "lt_languages",
         "key_field" => "id_lt_languages",
         "fields" => array("name"),
         "show_field" => "%1",
         "condition" => "",
         "not_in_create" => "true",
         "not_in_show" => "true",
//         "no_order" => "true",
         "order" => array ("orders" => "ASC"),
       ),

  "id_lt_menu" => array(
         "desc" => "Меню сайта",
         "type" => "select_from_table",
         "table" => "lt_menu",
         "key_field" => "id_lt_menu",
         "fields" => array("name"),
         "show_field" => "%1",
         "condition" => "",
         "not_in_create" => "true",
         "not_in_show" => "true",
//         "no_order" => "true",
         "order" => array ("orders" => "ASC"),
       ),

  "parent" => array(
         "desc" => "Родитель",
         "type" => "select_from_tree_table",
         "table" => "lt_pages",
         "key_field" => "id_lt_pages",
         "parent_field" => "parent",
         "fields" => array("name"),
         "show_field" => "%1",
         "condition" => "",
         "use_empty" => true,
         "not_in_create" => "true",
         "not_in_show" => "true",
//         "no_order" => "true",
         "order" => array ("orders" => "ASC"),
       ),


  "chapter" => array(
         "desc" => "Выберите раздел",

         "full_desc" => "Здесь указывается будущее размещение документа осносительно уже существующих",

         "type" => "pages",
         "not_in_table" => "true",
         "no_condition" => "true",
         "no_order" => "true",
//         "select_on_edit" => true,
  ),

  "orders" => array(
         "desc" => "Порядок вывода",
         "type" => "order",
         "js_validation" => array(
           "js_not_empty" => "Поле должно быть не пустым!",
           "js_match" => array (
             "pattern" => "^[\d]+$",
             "flags" => "g",
             "error" => "Только цифры! От 0 до 99999!",
           ),
         ),
         "select_on_edit" => true,
       ),




  "name" => array(
         "desc" => "Название страницы",
         "type" => "text",
         "maxlength" => "255",
         "size" => "70",
         "js_validation" => array(
           "js_not_empty" => "Поле должно быть не пустым!",
         ),
         "select_on_edit" => true,
         "select_on_edit_disabled" => true,
       ),

  "url" => array(
         "desc" => "Идентификатор страницы",
         "type" => "text",

         "full_desc" => "Часть адреса страницы, по которой она будет доступна, только латинские символы, цифры и спецсимволы!",

         "maxlength" => "255",
         "size" => "70",
         "select_on_edit" => true,
         "get_from_field" => array(
           "field" => "name",
           "translate" => "latin",
           "case" => "strtolower",
         ),
         "js_validation" => array(
           "js_match" => array (
             "pattern" => "^[A-Za-z0-9_\-:/~]+$",
             "flags" => "g",
             "error" => "Только латинские символы, цифры и спецсимволы!",
           ),
         ),
         "select_on_edit" => true,
       ),



  "content" => array(
         "desc" => "Содержание",
         "type" => "editor",

         "select_desc" => 'только для статических страниц',

         "width" => "100%",
         "height" => "500",
         "select_on_edit" => true,
       ),

  "unixtime" => array(
         "desc" => "Дата создания или модификации",
         "type" => "date",

         "select_desc" => 'только для статических страниц',

         "new" => true,
         "select_on_edit" => true,
         "fields" => array(
                      "d" => array("name" => "День","size" => "2", "after" => "."),
                      "m" => array("name" => "Месяц","size" => "2", "after" => "."),
                      "Y" => array("name" => "Год","size" => "4"),
                      "H" => array("name" => "Часы","size" => "2", "after" => ":"),
                      "i" => array("name" => "Минуты","size" => "2"),
                      ),
       ),




  "content_top" => array(
         "desc" => "Содержание (вверху)",
         "type" => "editor",

         "select_desc" => 'только для динамических страниц',

         "blank_window" => true,
         "show" => array ("type" => "textarea","width" => "50","height" => "2", "style" => "width:100%"),
         // "width" => "700",
         // "height" => "500",
       ),

  "file" => array(
         "desc" => "PHP-код страницы",
         "type" => "text_file_on_disk",


         "select_desc" => 'только для динамических страниц',

         "file_name_from" => "url",
         "extension" => ".php",
         "not_in_table" => true,
         "no_order" => true,
         "no_condition" => true,
         "path" => "../modules/",
         "style" => "width:100%",
         "width" => "70",
         "height" => "20",
         "select_on_edit" => true,
       ),

  "content_bottom" => array(
         "desc" => "Содержание (внизу)",

         "select_desc" => 'только для динамических страниц',

         "type" => "editor",
         "blank_window" => true,
         "show" => array ("type" => "textarea","width" => "50","height" => "2", "style" => "width:100%"),
         "width" => "700",
         // "height" => "500",
       ),




  "name_title" => array(
         "desc" => "Название страницы в заголовке",
         "type" => "text",

         "select_desc" => 'для всех типов страниц, кроме "Без содержания"',

         "maxlength" => "255",
         "size" => "70",
         "select_on_edit" => true,
         "get_from_field" => array(
           "field" => "name",
         ),

       ),

  "classification" => array(
         "desc" => "Классификация страницы",
         "type" => "text",

         "select_desc" => 'для всех типов страниц, кроме "Без содержания"',

         "maxlength" => "255",
         "size" => "70",
       ),

  "descrip" => array(
         "desc" => "Описание страницы",
         "type" => "textarea",

         "select_desc" => 'для всех типов страниц, кроме "Без содержания"',

         "width" => "70",
         "height" => "5",
         "select_on_edit" => true,
       ),

  "keywords" => array(
         "desc" => "Ключевые слова (через запятую)",
         "type" => "textarea",

         "select_desc" => 'для всех типов страниц, кроме "Без содержания"',

         "width" => "70",
         "height" => "5",
         "select_on_edit" => true,
       ),

  "name_menu" => array(
         "desc" => "Название пункта в меню",
         "type" => "text",

         "full_desc" => "Будет использоваться, только если страница учавствует в меню любого раздела, иначе можно оставить пустым",

         "select_desc" => 'если учавствует в меню',

         "maxlength" => "255",
         "size" => "70",
         "select_on_edit" => true,
         "get_from_field" => array(
           "field" => "name",
         ),

       ),

  "access" => array(
         "desc" => "Доступ к этой странице",
         "type" => "radio",
         "values" => array ("public" => "Всем", /*"auth" => "Авторизированным", */"admin" => "Только после авторизации в Админке"),
         "default_value" => "public",
       ),



  "target" => array(
         "desc" => "Открывать ссылку",
         "type" => "radio",
         "values" => array ("_self" => "В этом окне", "_blank" => "В новом окне"),
         "default_value" => "_self",
         "select_desc" => 'если учавствует в меню',
       ),

);
?>