<?
$config=array(
	"name"		=> "Страницы сайта",
	"menu_icon"	=>'icon-docs',
	"status"	=> "system",
	"windows"	=> array(
		"create"	=> array("width" => 1000,"height" => 700),
		"edit"		=> array("width" => 1000,"height" => 500),
	),
	"right" => array("admin","#GRANTED"),
	"main_table" => "lt_pages",
	"list"				=> array(
		"name"				=> array("isLink" => true),
		"url"				=> array(),
		"id_lt_languages"	=> array("align" => "center"),
	),
	"select" => array(
		"max_perpage" => 200,
		"default_orders" => array(
			array("parent" => "ASC"),
			array("name" => "ASC")
		),
		"default" => array(
			"id_lt_pages" => array(
				"desc" => "Выберите страницу(ы)",
				"type" => "pages",
			),
			"id_lt_languages" => array(
				"desc"			=> "Язык страницы",
				"type"			=> "select_from_table",
				"table"			=> "lt_languages",
				"key_field"		=> "id_lt_languages",
				"fields"		=> array("name"),
				"show_field"	=> "%1",
				"condition"		=> "",
				"order"			=> array ("orders" => "ASC"),
				"use_empty"		=> true,
			),
			"id_lt_menu" => array(
				"desc"			=> "Меню сайта",
				"type"			=> "select_from_table",
				"table"			=> "lt_menu",
				"key_field"		=> "id_lt_menu",
				"fields"		=> array("name"),
				"show_field"	=> "%1",
				"condition"		=> "",
				"order"			=> array ("orders" => "ASC"),
				"use_empty"		=> true,
			),
		),
	),
);
$actions=array(
	"create" => array(
        "before_code" => "create.php",
		"after_code" => "after_create.php",
	),
	"edit" => array(
		"before_code" => "edit.php",
	),
);

?>