<?
foreach ($form as $key => $value)
{
    if ( !empty($form[$key]['chapter']) )
    {
        $form[$key]['chapter']         =explode(",",$form[$key]['chapter']);
        $form[$key]['id_lt_languages'] =$form[$key]['chapter'][0];
        $form[$key]['id_lt_menu']      =$form[$key]['chapter'][1];
        $form[$key]['parent']            =( !empty($form[$key]['chapter'][2]) ? $form[$key]['chapter'][2] : 0 );
        unset($form[$key]['chapter']);
    }
    if ($form[$key]['page_type']!="dynamic") 
    {
        unset($form[$key]['file']);
        $form[$key]['content_top']="";
        $form[$key]['content_bottom']="";
        $get=$db->SelectSet("lt_pages","page_type,url","id_lt_pages=".$key);
        if ($db->count>0 && $get[0]["page_type"]=="dynamic")
        {
            @unlink("../modules/".$get[0]["url"]);
        }
    }
    if ($form[$key]['page_type']=="dynamic") 
    {
        $form[$key]['content']="";
    }
}
